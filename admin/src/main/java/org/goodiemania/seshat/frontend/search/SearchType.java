package org.goodiemania.seshat.frontend.search;

public enum SearchType {
    ITEMS,
    WORKS
}
