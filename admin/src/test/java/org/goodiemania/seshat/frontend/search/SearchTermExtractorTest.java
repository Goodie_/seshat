package org.goodiemania.seshat.frontend.search;

import java.util.List;
import org.goodiemania.odin.external.model.SearchTerm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SearchTermExtractorTest {
    private SearchTermExtractor searchTermExtractor;

    @BeforeEach
    public void setup() {
        searchTermExtractor = new SearchTermExtractor();
    }

    @Test
    public void basicTest() {
        List<SearchTerm> extract = searchTermExtractor.createTokens("Hello");
        Assertions.assertEquals(extract.size(), 1);
        Assertions.assertEquals(extract.get(0).getFieldName(), "%");
        Assertions.assertEquals(extract.get(0).getFieldValue(), "%Hello%");
    }

    @Test
    public void basicTestWithQuotes() {
        List<SearchTerm> extract = searchTermExtractor.createTokens("\"Hello there\"");
        Assertions.assertEquals(extract.size(), 1);
        Assertions.assertEquals(extract.get(0).getFieldName(), "%");
        Assertions.assertEquals(extract.get(0).getFieldValue(), "%Hello there%");
    }

    @Test
    public void basicTestWithFieldAndQuotes() {
        List<SearchTerm> extract = searchTermExtractor.createTokens("quote:\"Hello there\"");
        Assertions.assertEquals(extract.size(), 1);
        Assertions.assertEquals("%quote%", extract.get(0).getFieldName());
        Assertions.assertEquals("%Hello there%", extract.get(0).getFieldValue());
    }

    @Test
    public void basicTestWithField() {
        List<SearchTerm> extract = searchTermExtractor.createTokens("quote:Hello");
        Assertions.assertEquals(extract.size(), 1);
        Assertions.assertEquals("%quote%", extract.get(0).getFieldName());
        Assertions.assertEquals("%Hello%", extract.get(0).getFieldValue());
    }

    @Test
    public void multiWordTest() {
        List<SearchTerm> extract = searchTermExtractor.createTokens("quote:\"Hello there\" yo title:howdy");
        Assertions.assertEquals(extract.size(), 3);
        Assertions.assertEquals("%quote%", extract.get(0).getFieldName());
        Assertions.assertEquals("%Hello there%", extract.get(0).getFieldValue());
        Assertions.assertEquals("%", extract.get(1).getFieldName());
        Assertions.assertEquals("%yo%", extract.get(1).getFieldValue());
        Assertions.assertEquals("%title%", extract.get(2).getFieldName());
        Assertions.assertEquals("%howdy%", extract.get(2).getFieldValue());
    }
}