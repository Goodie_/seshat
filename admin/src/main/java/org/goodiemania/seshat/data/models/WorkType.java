package org.goodiemania.seshat.data.models;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.misc.WorkTypeField;

public class WorkType {
    private final String id;
    private final String name;
    private final String description;
    private final List<WorkTypeField> attributes;
    private final List<WorkTypeField> metadata;
    private final String attributeName;
    private final String attributeDesc;
    private final ConnectorInfo connectorInfo;


    /**
     * Constructs a new ItemType, given the field parameters.
     *
     * @param id                Id of the object
     * @param name              Name of the ItemType
     * @param description       Description of the ItemType
     * @param attributes        Required attributes of the WorkType
     * @param metadata          Required metadata of the Item
     * @param attributeName     Attribute that will be used as a works name
     * @param attributeDesc     Attribute that will be used as a works description
     * @param connectorInfo     Information about connecting to a connector
     */
    public WorkType(
            final String id,
            final String name,
            final String description,
            final List<WorkTypeField> attributes,
            final List<WorkTypeField> metadata,
            final String attributeName,
            final String attributeDesc,
            final ConnectorInfo connectorInfo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.attributes = attributes;
        this.metadata = metadata;
        this.attributeName = attributeName;
        this.attributeDesc = attributeDesc;
        this.connectorInfo = connectorInfo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<WorkTypeField> getAttributes() {
        return attributes;
    }

    public String getDescription() {
        return description;
    }

    public List<WorkTypeField> getMetadata() {
        return metadata;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getAttributeDesc() {
        return attributeDesc;
    }

    public Optional<ConnectorInfo> getConnectorInfo() {
        return Optional.ofNullable(connectorInfo);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final WorkType workType = (WorkType) o;

        return Objects.equals(id, workType.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
