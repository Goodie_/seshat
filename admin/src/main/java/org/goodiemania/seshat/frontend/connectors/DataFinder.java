package org.goodiemania.seshat.frontend.connectors;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;

public final class DataFinder {
    private final List<String> preferenceOrder;

    public DataFinder(final List<String> preferenceOrder) {
        this.preferenceOrder = preferenceOrder;
    }

    /**
     * Takes a list of found BookData, searches through it looking for the data given a preference list.
     *
     * @param providedData List of all book data found
     * @param supplier     Returns a field from the provided data
     * @param <T>          The object type of the data
     * @return Optional containing data from your preferred source
     */
    public <T> Optional<T> processBookData(final List<T> providedData, final Function<T, String> supplier) {
        for (String source : preferenceOrder) {
            Optional<T> data = providedData.stream()
                    .filter(givenData -> StringUtils.equals(source, supplier.apply(givenData)))
                    .findFirst();
            if (data.isPresent()) {
                return data;
            }
        }

        return Optional.empty();
    }
}
