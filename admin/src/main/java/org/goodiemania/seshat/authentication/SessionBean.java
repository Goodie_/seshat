package org.goodiemania.seshat.authentication;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.misc.ConnectorMapping;
import org.goodiemania.seshat.data.misc.User;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.goodiemania.seshat.data.models.WorkType;
import org.slf4j.Logger;

@Named
@SessionScoped
public class SessionBean implements Serializable {
    @Inject
    private WorkDao workDao;

    @Inject
    private Logger logger;

    private User user;

    /**
     * Attempts to load a user based on a given UserID.
     *
     * @param userId UserId to attempt to load
     */
    public void loadUser(String userId) {
        this.user = new User(userId, "Goodie", true, true, true);

        if (workDao.getAllWorkTypes().isEmpty()) {
            logger.info("Initializing database");

            WorkType bookWorkType = new WorkType(
                    UUID.randomUUID().toString(),
                    "Book",
                    "This is a book desc",
                    List.of(
                            new WorkTypeField("series-title", WorkTypeField.Type.STRING),
                            new WorkTypeField("title", WorkTypeField.Type.STRING),
                            new WorkTypeField("sub-title", WorkTypeField.Type.STRING),
                            new WorkTypeField("description", WorkTypeField.Type.STRING),
                            new WorkTypeField("Author(s)", WorkTypeField.Type.LIST),
                            new WorkTypeField("ISBN10", WorkTypeField.Type.STRING),
                            new WorkTypeField("ISBN13", WorkTypeField.Type.STRING)
                    ),
                    List.of(
                            new WorkTypeField("quality", WorkTypeField.Type.STRING)
                    ),
                    "title",
                    "description",
                    new ConnectorInfo(
                            "Book Information",
                            List.of(
                                    new ConnectorMapping("series-title", "series-title"),
                                    new ConnectorMapping("title", "title"),
                                    new ConnectorMapping("sub-title", "sub-title"),
                                    new ConnectorMapping("description", "description"),
                                    new ConnectorMapping("ISBN10", "ISBN10"),
                                    new ConnectorMapping("ISBN13", "ISBN13"),
                                    new ConnectorMapping("author", "Author(s)")
                            ),
                            Map.of("ISBN", List.of("ISBN10", "ISBN13"))
                    ));
            workDao.save(bookWorkType);
            workDao.save(new WorkType(
                    UUID.randomUUID().toString(),
                    "Vinyl Record",
                    "Vinyl, the old school hipster way to play music",
                    List.of(
                            new WorkTypeField("title", WorkTypeField.Type.STRING),
                            new WorkTypeField("description", WorkTypeField.Type.STRING),
                            new WorkTypeField("Artist(s)", WorkTypeField.Type.LIST)
                    ),
                    List.of(
                            new WorkTypeField("quality", WorkTypeField.Type.STRING)
                    ),
                    "title",
                    "description",
                    null));

            logger.info("Finished initializing database");
        }
    }

    public void logout() {
        this.user = null;
    }

    public User getUser() {
        return user;
    }

    public boolean isItemReadOnly() {
        return !user.isCanEditItems();
    }

    public boolean isWorkReadOnly() {
        return !user.isCanEditWorkTypes();
    }

    public boolean isWorkTypeReadOnly() {
        return !user.isCanEditWorkTypes();
    }
}
