package org.goodiemania.seshat.frontend.worktypes;

import java.io.Serializable;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.goodiemania.seshat.frontend.worktypes.model.ChooseConnectorSearchFields;
import org.goodiemania.seshat.frontend.worktypes.model.ConnectorMapping;
import org.omnifaces.util.Faces;

@Named
@ConversationScoped
public class ViewTypeController implements Serializable {
    @Inject
    private ViewTypeBean viewTypeBean;

    @Inject
    private WorkDao workDao;

    public void removeConnectorMapping(final ConnectorMapping connectorMapping) {
        viewTypeBean.getConnectorMappings().remove(connectorMapping);
    }

    public void removeAttribute(final WorkTypeField attribute) {
        viewTypeBean.getAttributes().remove(attribute);
    }


    public void removeMetadata(final WorkTypeField metadata) {
        viewTypeBean.getMetadata().remove(metadata);
    }

    public void removeConnectorSearch(final ChooseConnectorSearchFields fields) {
        viewTypeBean.getSearchFields().remove(fields);
    }

    /**
     * Adds the currently entered attribute type to the bean.
     */
    public void addAttribute() {
        if (StringUtils.isNotBlank(viewTypeBean.getNewAttrbName())) {
            WorkTypeField newAttribute = new WorkTypeField(
                    viewTypeBean.getNewAttrbName(),
                    viewTypeBean.getNewAttrbType());
            viewTypeBean.getAttributes().add(newAttribute);
            viewTypeBean.resetNewAttribute();
        }
    }

    /**
     * Adds the currently entered connector mapping to the bean.
     */
    public void addConnectorMapping() {
        boolean validFields = StringUtils.isNoneBlank(
                viewTypeBean.getNewConnectConnectName(),
                viewTypeBean.getNewConnectWorkName());
        if (validFields) {
            ConnectorMapping mapping = ConnectorMapping.of(
                    viewTypeBean.getNewConnectConnectName(),
                    viewTypeBean.getNewConnectWorkName()
            );
            viewTypeBean.getConnectorMappings().add(mapping);
            viewTypeBean.resetNewConnect();
        }
    }

    /**
     * Adds the currently entered connector mapping to the bean.
     */
    public void addConnectorSearch() {
        ChooseConnectorSearchFields fields = ChooseConnectorSearchFields.of(
                viewTypeBean.getNewSearchId(),
                viewTypeBean.getNewSearchFields()
        );
        viewTypeBean.getSearchFields().add(fields);
        viewTypeBean.resetNewConnectSearch();
    }

    /**
     * Adds the currently entered metadata type to the bean.
     */
    public void addMetadata() {
        if (StringUtils.isNotBlank(viewTypeBean.getNewMetaName())) {
            WorkTypeField newAttribute = new WorkTypeField(
                    viewTypeBean.getNewMetaName(),
                    viewTypeBean.getNewMetaType());
            viewTypeBean.getMetadata().add(newAttribute);
            viewTypeBean.resetNewMetadata();
        }
    }

    /**
     * Saves Whatever is currently in the add new type bean.
     *
     * @return String to redirect the browser with
     */
    public String save() {
        workDao.save(viewTypeBean.toWorkType());

        Faces.getContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Work Type updated", "")
        );
        Faces.getExternalContext().getFlash().setKeepMessages(true);

        return "browse?faces-redirect=true";
    }
}
