package org.goodiemania.seshat.data.models;

import java.util.List;
import java.util.Map;

public class Work {
    private final String id;
    private final WorkType workType;
    private final Map<String, Object> attributes;
    private final List<String> imageIds;


    /**
     * Constructs a new Work given the field parameters.
     *
     * @param id         Id of the object
     * @param workType   Item Type
     * @param attributes Attributes driven by the Item Type
     * @param imageIds   List of image Ids
     */
    public Work(
            final String id,
            final WorkType workType,
            final Map<String, Object> attributes,
            final List<String> imageIds) {
        this.id = id;
        this.workType = workType;
        this.attributes = attributes;
        this.imageIds = imageIds;
    }

    public String getId() {
        return id;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public String getName() {
        return attributes.get(workType.getAttributeName()).toString();
    }

    public String getDesc() {
        return attributes.get(workType.getAttributeDesc()).toString();
    }

    public List<String> getImageIds() {
        return imageIds;
    }
}
