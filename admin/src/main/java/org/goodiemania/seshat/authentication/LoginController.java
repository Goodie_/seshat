package org.goodiemania.seshat.authentication;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Named;

/**
 * Created on 9/11/2018.
 */
@Named
public class LoginController implements Serializable {
    public String login() throws IOException {
        return "/callback?faces-redirect=true";
    }
}
