package org.goodiemania.seshat.frontend.connectors.book.models;

public class Image {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}