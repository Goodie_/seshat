package org.goodiemania.odin.external.exceptions;

public class EntityException extends IllegalStateException {
    public EntityException(final String s) {
        super(s);
    }
}
