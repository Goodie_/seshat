package org.goodiemania.seshat.frontend.search;

public class SearchResult {
    private final String imageUrl;
    private final String name;
    private final String typeName;
    private final String id;

    /**
     * Creates a new instance of search result object.
     *
     * @param imageUrl Image URL to use for the cover photo
     * @param name     Name to be displayed
     * @param typeName Type (eg book)
     */
    public SearchResult(final String imageUrl, final String name, final String typeName, final String id) {
        this.name = name;
        this.typeName = typeName;
        this.id = id;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getId() {
        return id;
    }
}
