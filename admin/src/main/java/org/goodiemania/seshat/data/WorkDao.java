package org.goodiemania.seshat.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.goodiemania.odin.external.EntityManager;
import org.goodiemania.odin.external.Odin;
import org.goodiemania.odin.external.model.SearchTerm;
import org.goodiemania.seshat.data.dto.ImageDto;
import org.goodiemania.seshat.data.dto.ItemDto;
import org.goodiemania.seshat.data.dto.WorkDto;
import org.goodiemania.seshat.data.dto.WorkTypeDto;
import org.goodiemania.seshat.data.models.Image;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.search.SearchTermExtractor;
import org.slf4j.Logger;

@ApplicationScoped
public class WorkDao {
    @Inject
    private Logger logger;

    private EntityManager<WorkTypeDto> workTypeEm;
    private EntityManager<WorkDto> workEm;
    private EntityManager<ItemDto> itemEm;
    private EntityManager<ImageDto> imageEm;
    private DtoConverter dtoConverter;

    /**
     * Post construct to be called by CDI after the injected fields have been initialized.
     */
    @PostConstruct
    public void init() {
        Odin odin = Odin.create()
                .addPackageName("org.goodiemania.seshat.data.dto")
                .setJdbcConnectUrl("jdbc:sqlite:opt/database")
                .setObjectMapper(
                        new ObjectMapper()
                                .registerModule(new Jdk8Module()))
                .build();

        workTypeEm = odin.createFor(WorkTypeDto.class);
        workEm = odin.createFor(WorkDto.class);
        itemEm = odin.createFor(ItemDto.class);
        imageEm = odin.createFor(ImageDto.class);

        dtoConverter = new DtoConverter(workTypeEm, workEm);
    }

    /**
     * Saves the given ItemType.
     *
     * @param workType WorkType object to save
     */
    public void save(final WorkType workType) {
        workTypeEm.save(dtoConverter.from(workType));
    }

    /**
     * Saves the given Item.
     *
     * @param work Work object to save
     */
    public void save(final Work work) {
        workEm.save(dtoConverter.from(work));
    }

    /**
     * Saves the given Item.
     *
     * @param item Item object to save
     */
    public void save(final Item item) {
        WorkDto workDto = workEm.getById(item.getWork().getId())
                .orElseThrow();

        itemEm.saveWithAdditionalSearchParams(dtoConverter.from(item), workDto);
    }


    /**
     * Saves the given Image.
     *
     * @param image Image object to save
     */
    public void save(final Image image) {
        imageEm.save(dtoConverter.from(image));
    }


    /**
     * Return all Item Types in the store.
     *
     * @return List of Item Types
     */
    public List<WorkType> getAllWorkTypes() {
        return workTypeEm.getAll()
                .stream()
                .map(dtoConverter::from)
                .collect(Collectors.toList());
    }

    /**
     * Return a item given the ID, optionally.
     *
     * @param id Id of the potential Item
     * @return Optional of the found Item
     */
    public Optional<Item> getItemById(final String id) {
        return itemEm.getById(id)
                .map(dtoConverter::from);
    }

    /**
     * Return a item given the ID, optionally.
     *
     * @param id Id of the potential Item
     * @return Optional of the found Item
     */
    public Optional<Image> getImageById(final String id) {
        return imageEm.getById(id)
                .map(dtoConverter::from);
    }

    /**
     * Return a item given the ID, optionally.
     *
     * @param id Id of the potential Item
     * @return Optional of the found Item
     */
    public Optional<Work> getWorkById(final String id) {
        return workEm.getById(id)
                .map(dtoConverter::from);
    }

    /**
     * Return a item type given the ID, optionally.
     *
     * @param id Id of the potential Item Type
     * @return Optional of the found Item Type
     */
    public Optional<WorkType> getWorkTypeById(final String id) {
        return workTypeEm.getById(id)
                .map(dtoConverter::from);
    }

    /**
     * Search for Works in the database.
     *
     * @param value List of search terms
     * @return List of Works
     */
    public List<Work> searchWorks(final List<SearchTerm> value) {
        return workEm.search(value)
                .stream()
                .map(dtoConverter::from)
                .collect(Collectors.toList());
    }

    /**
     * Search for Works in the database.
     *
     * @param value The text value to search for
     * @return List of Items
     */
    public List<Work> searchWorks(final String value) {
        return searchWorks(new SearchTermExtractor().createTokens(value));
    }

    /**
     * Search for Items in the database.
     *
     * @param value List of search terms
     * @return List of Items
     */
    public List<Item> searchItems(final List<SearchTerm> value) {
        return itemEm.search(value)
                .stream()
                .map(dtoConverter::from)
                .collect(Collectors.toList());
    }

    /**
     * Search for Items in the database.
     *
     * @param value The text value to search for
     * @return List of Items
     */
    public List<Item> searchItems(final String value) {
        return searchItems(new SearchTermExtractor().createTokens(value));
    }

    /**
     * Empties the database.
     */
    public void emptyDatabase() {
        workTypeEm.getAll().forEach(workTypeDto -> workTypeEm.deleteById(workTypeDto.getId()));
        workEm.getAll().forEach(workDto -> workEm.deleteById(workDto.getId()));
        itemEm.getAll().forEach(itemDto -> itemEm.deleteById(itemDto.getId()));
    }
}
