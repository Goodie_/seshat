package org.goodiemania.seshat.data.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.goodiemania.odin.external.annotations.Entity;
import org.goodiemania.odin.external.annotations.Id;

@Entity
public class ImageDto {
    @Id
    private final String id;
    private final List<String> contentTypeHeaders;
    private final byte[] body;

    /**
     * Constructs a new ImageDto object with the given parameters.
     *
     * @param id                 Id of the object
     * @param contentTypeHeaders Content type headers at time of capture
     * @param body               Byte blob body
     */
    @JsonCreator
    public ImageDto(
            @JsonProperty("id") final String id,
            @JsonProperty("contentTypeHeaders") final List<String> contentTypeHeaders,
            @JsonProperty("body") final byte[] body) {
        this.id = id;
        this.contentTypeHeaders = contentTypeHeaders;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public List<String> getContentTypeHeaders() {
        return contentTypeHeaders;
    }

    public byte[] getBody() {
        return body;
    }
}
