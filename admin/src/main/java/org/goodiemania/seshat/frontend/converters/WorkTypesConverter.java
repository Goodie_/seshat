package org.goodiemania.seshat.frontend.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.models.WorkType;

@FacesConverter(forClass = WorkType.class, managed = true)
public class WorkTypesConverter implements Converter<WorkType> {
    @Inject
    private WorkDao workDao;

    @Override
    public WorkType getAsObject(final FacesContext facesContext, final UIComponent uiComponent, final String value) {
        return workDao.getWorkTypeById(value)
                .orElseThrow(() -> {
                    FacesMessage msg = new FacesMessage("Invalid work type");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);

                    return new ConverterException(msg);
                });
    }

    @Override
    public String getAsString(final FacesContext facesContext, final UIComponent uiComponent, final WorkType workType) {
        return workType.getId();
    }
}
