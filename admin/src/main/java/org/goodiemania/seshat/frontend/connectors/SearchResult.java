package org.goodiemania.seshat.frontend.connectors;

import java.util.List;
import java.util.Map;

public class SearchResult {
    private final Map<String, String> searchResults;
    private final Map<String, List<String>> listSearchResults;
    private final List<String> imageSearchResults;
    private final String source;


    private SearchResult(
            final Map<String, String> searchResults,
            final Map<String, List<String>> listSearchResults,
            final List<String> imageSearchResults, final String source) {
        this.searchResults = searchResults;
        this.listSearchResults = listSearchResults;
        this.imageSearchResults = imageSearchResults;
        this.source = source;
    }

    public static SearchResult of(
            final Map<String, String> searchResults,
            final Map<String, List<String>> listSearchResults,
            final List<String> imageSearchResults,
            final String source) {
        return new SearchResult(searchResults, listSearchResults, imageSearchResults, source);
    }

    public Map<String, String> getSearchResults() {
        return searchResults;
    }

    public Map<String, List<String>> getListSearchResults() {
        return listSearchResults;
    }

    public List<String> getImageSearchResults() {
        return imageSearchResults;
    }

    public String getSource() {
        return source;
    }
}
