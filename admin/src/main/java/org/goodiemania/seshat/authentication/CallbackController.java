package org.goodiemania.seshat.authentication;

import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created on 9/11/2018.
 */
@Named
@RequestScoped
public class CallbackController {
    @Inject
    private SessionBean sessionBean;

    /**
     * When the user is returned from whatever SSO they go through we process that here.
     *
     * <p>Please note, we are currently doing no SSO</p>
     *
     * @return Returns the redirection string, for home (or a error page)
     */
    public String callBack() {
        sessionBean.loadUser(UUID.randomUUID().toString());

        return "home?faces-redirect=true";
    }
}
