package org.goodiemania.seshat.data.misc;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

public class ConnectorInfo {
    private final String name;
    private final List<ConnectorMapping> connectorMapping;
    private final Map<String, List<String>> searchMapping;

    /**
     * Creates a new instance of Connector Info.
     *
     * @param name             Name of the connector
     * @param connectorMapping Mapping what the connector returns, to what goes into the work
     * @param searchMapping    Mapping of a connector search field to the local work field
     */
    @JsonCreator
    public ConnectorInfo(
            @JsonProperty("name") final String name,
            @JsonProperty("connectorMapping") final List<ConnectorMapping> connectorMapping,
            @JsonProperty("searchMapping") final Map<String, List<String>> searchMapping) {
        this.name = name;
        this.connectorMapping = connectorMapping;
        this.searchMapping = searchMapping;
    }

    public String getName() {
        return name;
    }

    public List<ConnectorMapping> getConnectorMapping() {
        return connectorMapping;
    }

    public Map<String, List<String>> getSearchMapping() {
        return searchMapping;
    }
}
