package org.goodiemania.seshat.frontend.connectors.book;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.goodiemania.seshat.frontend.connectors.Connector;
import org.goodiemania.seshat.frontend.connectors.SearchResult;
import org.goodiemania.seshat.frontend.connectors.book.models.BookInformation;
import org.goodiemania.seshat.frontend.connectors.book.models.BookResponse;
import org.goodiemania.seshat.frontend.connectors.book.models.Image;
import org.goodiemania.seshat.frontend.connectors.book.models.Isbn10;
import org.goodiemania.seshat.frontend.connectors.book.models.Isbn13;
import org.goodiemania.seshat.frontend.connectors.book.models.Title;
import org.goodiemania.seshat.misc.Properties;

public class BookConnector implements Connector {
    private static final Function<BookInformation, String> SOURCE_FINDER = bookInformation -> bookInformation.getSource().toString();
    private static final Map<String, Function<BookInformation, String>> FIELDS = Map.of(
            "series-title", bookInformation -> Optional.ofNullable(bookInformation.getTitle()).map(Title::getSeriesTitle).orElse(null),
            "title", bookInformation -> Optional.ofNullable(bookInformation.getTitle()).map(Title::getTitle).orElse(null),
            "sub-title", bookInformation -> Optional.ofNullable(bookInformation.getTitle()).map(Title::getSubTitle).orElse(null),
            "description", BookInformation::getDescription,
            "ISBN10", bookInformation -> Optional.ofNullable(bookInformation.getIsbn10()).map(Isbn10::getValue).orElse(null),
            "ISBN13", bookInformation -> Optional.ofNullable(bookInformation.getIsbn13()).map(Isbn13::getValue).orElse(null),
            "source", bookInformation -> bookInformation.getSource().toString()

    );
    private static final Map<String, Function<BookInformation, String>> IMAGE_FIELDS = Map.of(
            "cover_url", bookInformation -> Optional.ofNullable(bookInformation.getImage()).map(Image::getValue).orElse(null)
    );
    private static final Map<String, Function<BookInformation, List<String>>> LIST_FIELDS = Map.of(
            "author", bookInformation -> Optional.ofNullable(bookInformation.getAuthors()).orElse(Collections.emptySet())
                    .stream()
                    .map(author -> author.getName() + (StringUtils.isBlank(author.getDescription()) ? "" : " (" + author.getDescription() + ")"))
                    .collect(Collectors.toList())
    );
    private static final Set<String> SEARCH_TYPES = Set.of("ISBN");
    private static final String CONNECTOR_NAME = "Book Information";

    private final HttpClient httpClient;
    private final ObjectReader objectReader;

    public BookConnector() {
        httpClient = HttpClient.newBuilder().build();
        objectReader = new ObjectMapper().readerFor(BookResponse.class);
    }

    @Override
    public String getName() {
        return CONNECTOR_NAME;
    }

    @Override
    public Set<String> getSearchTypes() {
        return SEARCH_TYPES;
    }

    @Override
    public Map<String, WorkTypeField.Type> getFields() {
        HashMap<String, WorkTypeField.Type> stringTypeHashMap = new HashMap<>(FIELDS.keySet()
                .stream()
                .collect(Collectors.toMap(Function.identity(), s -> WorkTypeField.Type.STRING)));
        stringTypeHashMap.putAll(LIST_FIELDS.keySet()
                .stream()
                .collect(Collectors.toMap(Function.identity(), s -> WorkTypeField.Type.LIST)));

        return stringTypeHashMap;
    }

    @Override
    public List<SearchResult> search(final String searchType, final String searchTerm) {
        if (!SEARCH_TYPES.contains(searchType)) {
            throw new IllegalStateException("Search type must be part of searchTypes");
        }

        URI uri = URI.create(String.format("https://bookinformation.seshat.cc/v1/book?searchType=%s&searchTerm=%s", searchType, searchTerm));
        HttpRequest request = HttpRequest.newBuilder().GET()
                .uri(uri)
                .setHeader("authorization", Properties.BOOK_INFORMATION_API_KEY.get().orElseThrow())
                .build();

        try {
            HttpResponse<String> stringHttpResponse = makeRequest(request);

            if (stringHttpResponse.statusCode() != 200) {
                return Collections.emptyList();
            }

            BookResponse response = objectReader.readValue(stringHttpResponse.body());

            return response.getInfoList()
                    .stream()
                    .map(this::convert)
                    .collect(Collectors.toList());
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }

    }

    private SearchResult convert(final BookInformation info) {
        Map<String, List<String>> listFields = LIST_FIELDS.entrySet()
                .stream()
                .map(entry -> new KeyListOfStringHolder(entry.getKey(), entry.getValue().apply(info)))
                .collect(HashMap::new, (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll);

        Map<String, String> fields = FIELDS.entrySet()
                .stream()
                .map(entry -> new KeyStringHolder(entry.getKey(), entry.getValue().apply(info)))
                .collect(HashMap::new, (m, v) -> m.put(v.getKey(), v.getValue()), HashMap::putAll);

        List<String> imageFields = IMAGE_FIELDS.values()
                .stream()
                .map(bookInformationStringFunction -> bookInformationStringFunction.apply(info))
                .collect(Collectors.toList());

        String source = SOURCE_FINDER.apply(info);

        return SearchResult.of(fields, listFields, imageFields, source);
    }

    private HttpResponse<String> makeRequest(final HttpRequest request) {
        try {
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));
        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
