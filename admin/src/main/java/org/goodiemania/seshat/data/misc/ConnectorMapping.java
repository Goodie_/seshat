package org.goodiemania.seshat.data.misc;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConnectorMapping {
    private final String connectorName;
    private final String workName;

    /**
     * Creates a new instance of a connector mapping. I choose to make this a object over a map to ensure meaning.
     *
     * @param connectorName Name of value that comes from the connector
     * @param workName      Name of value for the work
     */
    @JsonCreator
    public ConnectorMapping(
            @JsonProperty("connectorName") final String connectorName,
            @JsonProperty("workName") final String workName) {
        this.connectorName = connectorName;
        this.workName = workName;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public String getWorkName() {
        return workName;
    }
}
