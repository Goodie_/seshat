package org.goodiemania.seshat.frontend.models.addwork;

import java.util.ArrayList;
import java.util.List;

public class AddWorkFieldPossibleData<T> {
    private List<String> sources;
    private boolean selected;
    private T value;

    /**
     * Creates a instance of AddWorkConnectorFieldData.
     *
     * @param source Source of the value
     * @param value  Value
     * @return AddWorkConnectorFieldData containing the data passed in
     */
    public static <T> AddWorkFieldPossibleData<T> of(final String source, final T value) {
        AddWorkFieldPossibleData<T> data = new AddWorkFieldPossibleData<>();

        ArrayList<String> sources = new ArrayList<>();
        sources.add(source);

        data.setSources(sources);
        data.setValue(value);
        data.setSelected(false);

        return data;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(final List<String> sources) {
        this.sources = sources;
    }

    public T getValue() {
        return value;
    }

    public void setValue(final T value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }
}
