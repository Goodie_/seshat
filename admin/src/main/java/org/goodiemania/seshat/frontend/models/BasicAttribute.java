package org.goodiemania.seshat.frontend.models;

public class BasicAttribute {
    private String name;
    private Object value;

    /**
     * Creates a new instance of BasicAttribute using the passed variables.
     *
     * @param name  Name of value, as per connector
     * @param value Value from the connector
     * @return created BasicAttribute
     */
    public static BasicAttribute of(final String name, final Object value) {
        BasicAttribute basicAttribute = new BasicAttribute();
        basicAttribute.setName(name);
        basicAttribute.setValue(value);
        return basicAttribute;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(final Object value) {
        this.value = value;
    }
}
