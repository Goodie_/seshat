package org.goodiemania.seshat.data.models;

import java.util.Map;

public class Item {
    private final String id;
    private final Work work;
    private final Map<String, Object> metaData;

    /**
     * Constructs a new Item given the field parameters.
     *
     * @param id       The ID of the Item
     * @param work     The associated work for this item
     * @param metaData The associated Metadata for this item
     */
    public Item(final String id, final Work work, final Map<String, Object> metaData) {
        this.id = id;
        this.work = work;
        this.metaData = metaData;
    }

    public String getId() {
        return id;
    }

    public Work getWork() {
        return work;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public String getName() {
        return work.getAttributes().get(work.getWorkType().getAttributeName()).toString();
    }

    public String getDesc() {
        return work.getAttributes().get(work.getWorkType().getAttributeDesc()).toString();
    }
}
