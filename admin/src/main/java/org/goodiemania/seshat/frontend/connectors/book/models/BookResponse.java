package org.goodiemania.seshat.frontend.connectors.book.models;

import java.util.List;

public class BookResponse {
    private List<BookInformation> infoList;

    public List<BookInformation> getInfoList() {
        return infoList;
    }

    public void setInfoList(final List<BookInformation> infoList) {
        this.infoList = infoList;
    }
}
