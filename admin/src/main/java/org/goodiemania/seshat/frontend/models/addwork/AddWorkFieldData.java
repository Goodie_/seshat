package org.goodiemania.seshat.frontend.models.addwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddWorkFieldData {
    private List<String> sources;
    private String value;

    /**
     * Creates a instance of AddWorkConnectorFieldData.
     *
     * @param source Source of the value
     * @param value  Value
     * @return AddWorkConnectorFieldData containing the data passed in
     */
    public static AddWorkFieldData of(final String source, final String value) {
        AddWorkFieldData data = new AddWorkFieldData();

        ArrayList<String> sources = new ArrayList<>();
        sources.add(source);

        data.setSources(sources);
        data.setValue(value);

        return data;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(final List<String> sources) {
        this.sources = sources;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AddWorkFieldData that = (AddWorkFieldData) o;
        return Objects.equals(sources, that.sources)
                && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sources, value);
    }
}
