package org.goodiemania.seshat.frontend.works;

import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.slf4j.Logger;

@Named
public class EditWorkController implements Serializable {
    @Inject
    private WorkDao workDao;

    @Inject
    private EditWorkBean editWorkBean;

    @Inject
    private Logger logger;

    /**
     * Saves Whatever is currently in the view item bean.
     *
     * @return String to redirect the browser with
     */
    public String save() {
        logger.info("Saving edited item");
        workDao.save(editWorkBean.toWork());
        return "/home?faces-redirect=true";
    }
}
