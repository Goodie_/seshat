package org.goodiemania.seshat.frontend.models;

public class KeyValueMap {
    private String key;
    private String value;

    /**
     * Creates a new instance of a KeyValueMap.
     *
     * @param key   key value
     * @param value value value
     * @return A mapping containing both.
     */
    public static KeyValueMap of(final String key, final String value) {
        KeyValueMap keyValueMap = new KeyValueMap();
        keyValueMap.setKey(key);
        keyValueMap.setValue(value);

        return keyValueMap;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
