package org.goodiemania.seshat.frontend.models;

import java.util.List;
import org.goodiemania.seshat.data.misc.WorkTypeField;

public class Attribute {
    private WorkTypeField attribute;
    private Object value;
    private String newListValue;

    public Attribute(final WorkTypeField attribute, final Object value) {
        this.attribute = attribute;
        this.value = value;
    }

    public WorkTypeField getAttribute() {
        return attribute;
    }

    public void setAttribute(final WorkTypeField attribute) {
        this.attribute = attribute;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

    public String getNewListValue() {
        return newListValue;
    }

    public void setNewListValue(final String newListValue) {
        this.newListValue = newListValue;
    }

    /**
     * If this contains a list it adds the newListValue to the list.
     */
    public void addNewListValue() {
        if (value instanceof List) {
            List list = (List) value;
            list.add(newListValue);
            newListValue = "";
        }
    }

    /**
     * If this contains a list it adds the newListValue to the list.
     */
    public void removeListValue(final Object listValue) {
        if (value instanceof List) {
            List list = (List) value;
            list.remove(listValue);
        }
    }
}
