package org.goodiemania.seshat.frontend.connectors.book.models;

public class Isbn13 {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
