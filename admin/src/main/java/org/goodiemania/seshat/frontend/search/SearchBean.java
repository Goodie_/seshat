package org.goodiemania.seshat.frontend.search;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.Param;

@Named
@ViewScoped
public class SearchBean implements Serializable {

    @Inject
    @Param(name = "q")
    private String searchString;
    @Inject
    @Param(name = "t")
    private SearchType searchType;

    private BigDecimal searchTime;
    private List<SearchResult> searchResults = Collections.emptyList();

    /**
     * Initializes bean.
     */
    @PostConstruct
    public void init() {
        searchResults = Collections.emptyList();
        searchTime = BigDecimal.ZERO;

        if (searchType == null) {
            searchType = SearchType.ITEMS;
        }

        if (searchString == null) {
            searchString = "";
        }
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(final String searchString) {
        this.searchString = searchString;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(final List<SearchResult> searchResults) {
        this.searchResults = searchResults;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(final SearchType searchType) {
        this.searchType = searchType;
    }

    public BigDecimal getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(final BigDecimal searchTime) {
        this.searchTime = searchTime;
    }
}
