package org.goodiemania.seshat.frontend.connectors.book;

final class KeyStringHolder {
    final String key;
    final String value;

    KeyStringHolder(String k, String v) {
        this.key = k;
        this.value = v;
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }
}