package org.goodiemania.seshat.frontend.items;

import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.frontend.NavigationController;
import org.slf4j.Logger;

@Named
public class EditItemController implements Serializable {
    @Inject
    private WorkDao workDao;

    @Inject
    private EditItemBean editItemBean;

    @Inject
    private Logger logger;

    @Inject
    private NavigationController navigationController;

    /**
     * Saves Whatever is currently in the edit item bean.
     *
     * @return String to redirect the browser with
     */
    public String save() {
        logger.info("Saving edited item");
        workDao.save(editItemBean.toItem());
        return navigationController.viewItem(editItemBean.getId());
    }
}
