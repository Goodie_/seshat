package org.goodiemania.seshat.frontend.converters;

import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkFieldData;

@FacesConverter(forClass = AddWorkFieldData.class, managed = true)
public class AddWorkFieldDataConverter implements Converter<AddWorkFieldData> {
    private static final String SEPARATOR = "%%%";
    @Inject
    private ConnectorsController connectorsController;

    @Override
    public AddWorkFieldData getAsObject(final FacesContext facesContext, final UIComponent uiComponent, final String value) {
        if (StringUtils.isBlank(value)) {
            FacesMessage msg = new FacesMessage("Invalid Work field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            throw new ConverterException(msg);
        }

        String[] split = value.split(SEPARATOR);

        if (split.length < 2) {
            FacesMessage msg = new FacesMessage("Invalid Work field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);

            throw new ConverterException(msg);
        }

        AddWorkFieldData data = new AddWorkFieldData();
        data.setValue(split[0]);
        data.setSources(new ArrayList<>());

        for (int i = 1; i < split.length; i++) {
            data.getSources().add(split[i]);
        }

        return data;
    }

    @Override
    public String getAsString(final FacesContext facesContext, final UIComponent uiComponent, final AddWorkFieldData connector) {
        return connector.getValue() + SEPARATOR + String.join(SEPARATOR, connector.getSources());
    }
}
