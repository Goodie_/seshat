package org.goodiemania.seshat.authentication;

import com.google.common.collect.Sets;
import java.io.IOException;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "Session filter", urlPatterns = "*")
public class SessionFilter implements Filter {
    private static final Set<String> UNPROTECTED_PATH =
            Sets.newHashSet("/index",
                    "/callback",
                    "/javax.faces.resource",
                    "/healthCheck");

    @Inject
    private SessionBean sessionBean;

    @Override
    public void doFilter(
            final ServletRequest servletRequest,
            final ServletResponse servletResponse,
            final FilterChain chain)
            throws IOException, ServletException {
        String partialPath = ((HttpServletRequest) servletRequest).getRequestURI();

        if (UNPROTECTED_PATH.stream().anyMatch(partialPath::startsWith)) {
            chain.doFilter(servletRequest, servletResponse);
        } else if (sessionBean.getUser() == null) {
            ((HttpServletResponse) servletResponse).sendRedirect("/index");
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
