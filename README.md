Seshat is intended to (eventually!) be a all purpose library management software.

Seshat tracks "items" and "work types". These types are intended to be just about anything, from books to trees.

We intend to be able to track various metadata against each work, such as location, created date, collected date.

Questions:
* Do we want metadata to also be customizable? Probably.