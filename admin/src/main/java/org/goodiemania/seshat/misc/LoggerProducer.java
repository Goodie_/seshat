package org.goodiemania.seshat.misc;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class LoggerProducer {
    /**
     * Returns a logger for the given injection point.
     *
     * @param ip Injection point, where this logger will go
     * @return Returns a logger for the declaring class at the injection point
     */
    @Produces
    public Logger producer(final InjectionPoint ip) {
        return LoggerFactory.getLogger(ip.getMember().getDeclaringClass().getName());
    }
}
