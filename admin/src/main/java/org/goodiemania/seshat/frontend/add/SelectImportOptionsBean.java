package org.goodiemania.seshat.frontend.add;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.goodiemania.seshat.data.models.Work;

@Named
@ConversationScoped
public class SelectImportOptionsBean implements Serializable {
    private List<Work> possibleWorks;

    public List<Work> getPossibleWorks() {
        return possibleWorks;
    }

    public void setPossibleWorks(final List<Work> possibleWorks) {
        this.possibleWorks = possibleWorks;
    }
}
