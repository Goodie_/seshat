package org.goodiemania.seshat.frontend.connectors;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

@ApplicationScoped
public class ConnectorsController {
    private List<Connector> connectors;

    private static List<Connector> getClassInstances(final Reflections reflections) {
        return reflections
                .getSubTypesOf(Connector.class)
                .stream()
                .map(classes -> classes.getConstructors()[0])
                .map(constructor -> {
                    try {
                        return (Connector) constructor.newInstance();
                    } catch (ClassCastException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        throw new IllegalStateException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void init() {
        connectors = getClassInstances(new Reflections("org.goodiemania.seshat.frontend.connectors"));
    }

    public List<Connector> getConnectors() {
        return connectors;
    }

    /**
     * Optionally gets a connector with a matching name.
     *
     * @param name Name of the connector to search for
     * @return Optional connector
     */
    public Optional<Connector> getByName(final String name) {
        return connectors.stream()
                .filter(connector -> StringUtils.equals(name, connector.getName()))
                .findFirst();
    }
}
