package org.goodiemania.seshat.frontend.models.addwork;

import java.util.List;

public class AddWorkField {
    private String fieldName;
    private AddWorkFieldData value;
    private List<AddWorkFieldData> possibleValues;

    /**
     * Creates a instance of AddWorkConnectorField.
     *
     * @param fieldName      Field name
     * @param possibleValues List of possible values for this field
     * @return AddWorkConnectorField containing said values
     */
    public static AddWorkField of(final String fieldName, final List<AddWorkFieldData> possibleValues) {
        AddWorkField field = new AddWorkField();
        field.setFieldName(fieldName);
        field.setPossibleValues(possibleValues);
        field.setValue(null);

        return field;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    public List<AddWorkFieldData> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(final List<AddWorkFieldData> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public AddWorkFieldData getValue() {
        return value;
    }

    public void setValue(final AddWorkFieldData value) {
        this.value = value;
    }
}
