package org.goodiemania.seshat.frontend.search;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.odin.external.model.SearchTerm;

public class SearchTermExtractor {

    private String newSearchField = "";
    private boolean inQuotes = false;
    private StringBuilder newSearchTerm = new StringBuilder();
    private List<SearchTerm> terms = new ArrayList<>();

    /**
     * Turns a single String into a set of tokens.
     *
     * @param originalString The original search string
     * @return A list of search strings
     */
    public List<SearchTerm> createTokens(final String originalString) {
        for (char character : originalString.toCharArray()) {
            if (inQuotes) {
                if (character == '"') {
                    createSearchTerm();
                } else {
                    newSearchTerm.append(character);
                }
            } else {
                if (character == '"') {
                    inQuotes = true;
                } else if (character == ' ') {
                    if (newSearchTerm.length() != 0) {
                        createSearchTerm();
                    }
                } else if (character == ':') {
                    newSearchField = newSearchTerm.toString();
                    newSearchTerm = new StringBuilder();
                } else {
                    newSearchTerm.append(character);
                }
            }
        }

        if (newSearchTerm.length() != 0) {
            createSearchTerm();
        }

        return terms;
    }

    private void createSearchTerm() {
        if (StringUtils.isBlank(newSearchField)) {
            newSearchField = "%";
        } else {
            newSearchField = String.format("%%%s%%", newSearchField);
        }
        newSearchTerm.insert(0, '%');
        newSearchTerm.append('%');
        terms.add(SearchTerm.of(newSearchField, newSearchTerm.toString()));

        inQuotes = false;
        newSearchField = "";
        newSearchTerm = new StringBuilder();
    }
}
