package org.goodiemania.seshat.data.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.List;

public class Image {
    private final String id;
    private final List<String> contentTypeHeaders;
    private final byte[] body;

    /**
     * Constructs a new Image object with the given parameters.
     *
     * @param id                 Id of the object
     * @param contentTypeHeaders Content type headers at time of capture
     * @param body               Byte blob body
     */
    @JsonCreator
    public Image(final String id, final List<String> contentTypeHeaders, final byte[] body) {
        this.id = id;
        this.contentTypeHeaders = contentTypeHeaders;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public List<String> getContentTypeHeaders() {
        return contentTypeHeaders;
    }

    public byte[] getBody() {
        return body;
    }
}
