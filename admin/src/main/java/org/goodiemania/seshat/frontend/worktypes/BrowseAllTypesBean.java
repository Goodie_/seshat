package org.goodiemania.seshat.frontend.worktypes;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.models.WorkType;

@Named
@RequestScoped
public class BrowseAllTypesBean implements Serializable {
    @Inject
    private WorkDao workDao;
    private List<WorkType> workTypes;

    @PostConstruct
    public void init() {
        workTypes = workDao.getAllWorkTypes();
    }

    public List<WorkType> getWorkTypes() {
        return workTypes;
    }
}
