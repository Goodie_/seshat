package org.goodiemania.seshat.frontend.models.addwork;

import java.util.List;

public class AddWorkListField {
    private String fieldName;
    private List<AddWorkFieldData> value;
    private List<AddWorkFieldData> possibleValues;

    /**
     * Creates a instance of AddWorkConnectorField.
     *
     * @param fieldName      Field name
     * @param possibleValues List of possible values for this field
     * @return AddWorkConnectorField containing said values
     */
    public static AddWorkListField of(final String fieldName, final List<AddWorkFieldData> possibleValues) {
        AddWorkListField field = new AddWorkListField();
        field.setFieldName(fieldName);
        field.setPossibleValues(possibleValues);
        field.setValue(null);

        return field;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    public List<AddWorkFieldData> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(final List<AddWorkFieldData> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public List<AddWorkFieldData> getValue() {
        return value;
    }

    public void setValue(final List<AddWorkFieldData> value) {
        this.value = value;
    }
}
