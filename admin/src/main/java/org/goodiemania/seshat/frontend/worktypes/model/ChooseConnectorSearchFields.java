package org.goodiemania.seshat.frontend.worktypes.model;

import java.util.List;

public class ChooseConnectorSearchFields {
    private String searchId;
    private List<String> searchFields;

    /**
     * Creates a new instance.
     *
     * @param searchId ID of the connector search
     * @return A mapping to a list of work fields
     */
    public static ChooseConnectorSearchFields of(final String searchId, final List<String> searchFields) {
        ChooseConnectorSearchFields field = new ChooseConnectorSearchFields();
        field.setSearchId(searchId);
        field.setSearchFields(searchFields);
        return field;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(final String searchId) {
        this.searchId = searchId;
    }

    public List<String> getSearchFields() {
        return searchFields;
    }

    public void setSearchFields(final List<String> searchFields) {
        this.searchFields = searchFields;
    }
}
