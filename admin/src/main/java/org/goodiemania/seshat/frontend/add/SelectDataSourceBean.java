package org.goodiemania.seshat.frontend.add;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.models.Image;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;
import org.goodiemania.seshat.frontend.connectors.SearchResult;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkField;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkFieldData;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkFieldPossibleData;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkListField;
import org.goodiemania.seshat.misc.Properties;

@Named
@ConversationScoped
public class SelectDataSourceBean implements Serializable {
    @Inject
    private ConnectorsController connectorsController;

    private List<AddWorkField> fields;
    private List<AddWorkListField> listFields;
    private List<AddWorkFieldPossibleData<String>> imageFields;

    private WorkType workType;
    private String searchType;
    private String searchTerm;


    /**
     * Loads the boan with the specified values, and searches in the specified workType connector for a result.
     *
     * @param workType   Work type to be run against
     * @param searchType Search type, to be used with the connector
     * @param searchTerm Seorch term to be used with the connector
     * @return If the connector found any values
     */
    public boolean load(final WorkType workType, final String searchType, final String searchTerm) {
        this.workType = workType;
        this.searchType = searchType;
        this.searchTerm = searchTerm;

        fields = new ArrayList<>();
        listFields = new ArrayList<>();
        imageFields = new ArrayList<>();

        List<SearchResult> searchResults = workType.getConnectorInfo()
                .map(ConnectorInfo::getName)
                .flatMap(connectorName -> connectorsController.getByName(connectorName))
                .map(connector -> connector.search(searchType, searchTerm))
                .orElse(Collections.emptyList());

        if (searchResults.isEmpty()) {
            return false;
        }

        searchResults
                .forEach(result -> {
                    String source = result.getSource();

                    convert(workType.getConnectorInfo().get(), result.getSearchResults())
                            .forEach((fieldName, value) -> dealWithSearchResult(fieldName, value, source));
                    convert(workType.getConnectorInfo().get(), result.getListSearchResults())
                            .forEach((fieldName, value) -> dealWithListSearchResult(fieldName, value, source));

                    result.getImageSearchResults().forEach(value -> dealWithImageSearchResult(value, source));
                });

        return true;
    }

    private void dealWithSearchResult(final String fieldName, final String value, final String source) {
        if (StringUtils.isBlank(value)) {
            return;
        }

        AddWorkField connectorField = fields.stream()
                .filter(field -> StringUtils.equals(fieldName, field.getFieldName()))
                .findFirst()
                .orElseGet(() -> {
                    AddWorkField field = AddWorkField.of(fieldName, new ArrayList<>());
                    fields.add(field);
                    return field;
                });

        connectorField.getPossibleValues().stream()
                .filter(data -> StringUtils.equals(data.getValue(), value))
                .findFirst()
                .ifPresentOrElse(data -> data.getSources().add(source),
                        () -> connectorField.getPossibleValues().add(AddWorkFieldData.of(source, value)));
    }

    private <T> Map<String, T> convert(final ConnectorInfo converterInfo, final Map<String, T> object) {
        HashMap<String, T> newMap = new HashMap<>();

        converterInfo.getConnectorMapping().forEach(connectorMapping -> {
            T t = object.get(connectorMapping.getConnectorName());
            newMap.put(connectorMapping.getWorkName(), t);
        });

        return newMap;
    }

    private void dealWithImageSearchResult(final String value, final String source) {
        if (StringUtils.isBlank(value)) {
            return;
        }

        imageFields.stream()
                .filter(data -> StringUtils.equals(data.getValue(), value))
                .findFirst()
                .ifPresentOrElse(data -> data.getSources().add(source),
                        () -> imageFields.add(AddWorkFieldPossibleData.of(source, value)));
    }

    private void dealWithListSearchResult(final String fieldName, final List<String> value, final String source) {
        if (value == null || value.isEmpty()) {
            return;
        }

        AddWorkListField connectorField = listFields.stream()
                .filter(field -> StringUtils.equals(fieldName, field.getFieldName()))
                .findFirst()
                .orElseGet(() -> {
                    AddWorkListField field = AddWorkListField.of(fieldName, new ArrayList<>());
                    listFields.add(field);
                    return field;
                });

        value.forEach(individualValue -> {
            connectorField.getPossibleValues().stream()
                    .filter(data -> StringUtils.equals(data.getValue(), individualValue))
                    .findFirst()
                    .ifPresentOrElse(data -> data.getSources().add(source),
                            () -> connectorField.getPossibleValues().add(AddWorkFieldData.of(source, individualValue)));
        });
    }


    public List<AddWorkListField> getListFields() {
        return listFields;
    }

    public List<AddWorkField> getFields() {
        return fields;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public String getSearchType() {
        return searchType;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public List<AddWorkFieldPossibleData<String>> getImageFields() {
        return imageFields;
    }
}
