package org.goodiemania.seshat.frontend.connectors;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.goodiemania.seshat.data.misc.WorkTypeField;

public interface Connector {
    String getName();

    Map<String, WorkTypeField.Type> getFields();

    Set<String> getSearchTypes();

    List<SearchResult> search(String searchType, String searchTerm);
}
