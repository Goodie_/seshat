package org.goodiemania.seshat.data.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;
import org.goodiemania.odin.external.annotations.Entity;
import org.goodiemania.odin.external.annotations.Id;
import org.goodiemania.odin.external.annotations.Index;

@Entity
@Index
public class WorkDto {
    @Id
    private final String id;
    private final String itemTypeId;
    private final Map<String, Object> attributes;
    private final List<String> imageIds;

    /**
     * Constructs a new ItemDto given the field parameters, annotated for Jacksons use.
     *
     * @param id         Id of the object
     * @param itemTypeId Item Type ID
     * @param attributes Attributes driven by the Item Type
     */
    @JsonCreator
    public WorkDto(
            @JsonProperty("id") final String id,
            @JsonProperty("itemTypeId") final String itemTypeId,
            @JsonProperty("attributes") final Map<String, Object> attributes,
            @JsonProperty("imageIds") final List<String> imageIds) {
        this.id = id;
        this.itemTypeId = itemTypeId;
        this.attributes = attributes;
        this.imageIds = imageIds;
    }

    public String getId() {
        return id;
    }

    public String getItemTypeId() {
        return itemTypeId;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public List<String> getImageIds() {
        return imageIds;
    }
}
