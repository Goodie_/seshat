package org.goodiemania.seshat.data.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import org.goodiemania.odin.external.annotations.Entity;
import org.goodiemania.odin.external.annotations.Id;
import org.goodiemania.odin.external.annotations.Index;

@Entity
@Index
public class ItemDto {
    @Id
    private final String id;
    private final String workId;
    private final Map<String, Object> metaData;

    /**
     * Constructs a new ItemDto given the field parameters, annotated for jacksons use.
     *
     * @param id       ID of the object
     * @param workId   ID of the work related to this item
     * @param metaData Metadata relating to this item
     */
    @JsonCreator
    public ItemDto(
            @JsonProperty("id") final String id,
            @JsonProperty("workId") final String workId,
            @JsonProperty("metaData") final Map<String, Object> metaData) {
        this.id = id;
        this.workId = workId;
        this.metaData = metaData;
    }

    public String getId() {
        return id;
    }

    public Map<String, Object> getMetaData() {
        return metaData;
    }

    public String getWorkId() {
        return workId;
    }
}
