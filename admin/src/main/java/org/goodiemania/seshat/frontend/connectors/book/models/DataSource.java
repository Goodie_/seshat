package org.goodiemania.seshat.frontend.connectors.book.models;

public enum DataSource {
    GOOGLE_BOOKS,
    GOOD_READS,
    OPEN_LIBRARY,
    LIBRARY_THING,
    SEARCH,
}
