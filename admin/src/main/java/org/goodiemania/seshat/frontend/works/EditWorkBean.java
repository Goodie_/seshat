package org.goodiemania.seshat.frontend.works;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.models.Attribute;

@Named
@ConversationScoped
public class EditWorkBean implements Serializable {
    private String id;
    private Work work;
    private WorkType workType;
    private List<Attribute> attributes = new ArrayList<>();
    private List<String> imageIds;

    /**
     * Load the specified Item into the bean.
     *
     * @param work Work object
     */
    public void load(final Work work) {
        this.id = work.getId();
        this.work = work;
        this.workType = work.getWorkType();

        this.attributes = new ArrayList<>();
        work.getWorkType().getAttributes().forEach(attribute -> {
            Object value = work.getAttributes().getOrDefault(attribute.getName(), attribute.getType().getDefaultSupplier().get());
            attributes.add(new Attribute(attribute, value));
        });

        this.imageIds = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public WorkType getWorkType() {
        return workType;
    }

    /**
     * Converts Bean fields to Item.
     *
     * @return Converted Item
     */
    public Work toWork() {
        Map<String, Object> attributes = this.attributes.stream()
                .collect(Collectors.toMap(attribute -> attribute.getAttribute().getName(), Attribute::getValue));

        return new Work(id, workType, attributes, imageIds);
    }

    public String getName() {
        return work.getAttributes().get(workType.getAttributeName()).toString();
    }

    public String getDesc() {
        return work.getAttributes().get(workType.getAttributeDesc()).toString();
    }
}
