package org.goodiemania.seshat.frontend;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Conversation;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.odin.external.model.SearchTerm;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.items.EditItemBean;
import org.goodiemania.seshat.frontend.items.ViewItemBean;
import org.goodiemania.seshat.frontend.search.SearchType;
import org.goodiemania.seshat.frontend.works.EditWorkBean;
import org.goodiemania.seshat.frontend.works.ViewWorkBean;
import org.goodiemania.seshat.frontend.worktypes.ViewTypeBean;

@Named
public class NavigationController implements Serializable {
    private static final String FACES_REDIRECT_TRUE = "faces-redirect=true";

    @Inject
    private WorkDao workDao;

    @Inject
    private Conversation conversation;

    @Inject
    private ViewTypeBean viewTypeBean;

    @Inject
    private ViewItemBean viewItemBean;

    @Inject
    private EditItemBean editItemBean;

    @Inject
    private ViewWorkBean viewWorkBean;

    @Inject
    private EditWorkBean editWorkBean;

    /**
     * Returns the correct redirection needed after you search for something.
     *
     * @param type           Type of search that was performed
     * @param searchResultId The Item to load
     * @return URI redirection path for viewing the Item
     */
    public String viewSearchResult(final SearchType type, final String searchResultId) {
        if (type == SearchType.ITEMS) {
            return viewItem(searchResultId);
        } else if (type == SearchType.WORKS) {
            return viewWork(searchResultId);
        }
        System.out.println("Hit a point I should never hit");
        return "Fuck.";
    }

    /**
     * Provides redirection path and loads the bean for viewing a Item.
     *
     * @param id The ID of the item to load
     * @return URI redirection path for viewing Item
     */
    public String viewItem(final String id) {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        Item foundItem = workDao.getItemById(id).orElseThrow();
        viewItemBean.load(foundItem);
        return String.format("/item/view?%s", FACES_REDIRECT_TRUE);
    }

    /**
     * Provides redirection path and loads the bean for viewing a Item.
     *
     * @param id The ID of the item to load
     * @return URI redirection path for viewing Item
     */
    public String editItem(final String id) {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        Item foundItem = workDao.getItemById(id).orElseThrow();
        editItemBean.load(foundItem);
        return String.format("/item/edit?%s", FACES_REDIRECT_TRUE);
    }

    /**
     * Provides redirection path and loads the bean for viewing a work.
     *
     * @param id The ID of the work to load
     * @return URI redirection path for viewing work
     */
    public String viewWork(final String id) {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        Work work = workDao.getWorkById(id).orElseThrow();
        List<Item> items = workDao.searchItems(List.of(SearchTerm.of("ItemDto_workId", work.getId())));
        viewWorkBean.load(work, items);

        //TODO implement
        return String.format("/work/view?%s", FACES_REDIRECT_TRUE);
    }

    /**
     * Provides redirection path and loads the bean for viewing a work.
     *
     * @param id The ID of the work to load
     * @return URI redirection path for viewing work
     */
    public String editWork(final String id) {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        Work work = workDao.getWorkById(id).orElseThrow();
        editWorkBean.load(work);

        //TODO implement
        return String.format("/work/edit?%s", FACES_REDIRECT_TRUE);
    }

    /**
     * Provides redirection path and loads the bean for viewing a work type.
     *
     * @param workTypeId The ID of the work type to load
     * @return URI redirection path for viewing work type
     */
    public String viewWorkType(final String workTypeId) {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        WorkType foundWorkType = workDao.getWorkTypeById(workTypeId).orElseThrow();

        viewTypeBean.load(foundWorkType);
        return String.format("/workTypes/view?%s", FACES_REDIRECT_TRUE);
    }

    public String addItem() {
        return "/add/init";
    }

    public String viewHome() {
        return "/home?faces-redirect=true";
    }
}
