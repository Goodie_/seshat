package org.goodiemania.seshat.frontend.models.addwork;

import java.util.List;

public class AddWorkImageField {
    private String fieldName;
    private List<AddWorkFieldPossibleData<String>> possibleValues;

    /**
     * Creates a instance of AddWorkConnectorField.
     *
     * @param fieldName      Field name
     * @param possibleValues List of possible values for this field
     * @return AddWorkConnectorField containing said values
     */
    public static AddWorkImageField of(final String fieldName, final List<AddWorkFieldPossibleData<String>> possibleValues) {
        AddWorkImageField field = new AddWorkImageField();
        field.setFieldName(fieldName);
        field.setPossibleValues(possibleValues);

        return field;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    public List<AddWorkFieldPossibleData<String>> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(final List<AddWorkFieldPossibleData<String>> possibleValues) {
        this.possibleValues = possibleValues;
    }
}
