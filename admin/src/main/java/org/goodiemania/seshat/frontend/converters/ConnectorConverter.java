package org.goodiemania.seshat.frontend.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import org.goodiemania.seshat.frontend.connectors.Connector;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;

@FacesConverter(forClass = Connector.class, managed = true)
public class ConnectorConverter implements Converter<Connector> {
    @Inject
    private ConnectorsController connectorsController;

    @Override
    public Connector getAsObject(final FacesContext facesContext, final UIComponent uiComponent, final String value) {
        return connectorsController.getByName(value)
                .orElseThrow(() -> {
                    FacesMessage msg = new FacesMessage("Invalid connector");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);

                    return new ConverterException(msg);
                });
    }

    @Override
    public String getAsString(final FacesContext facesContext, final UIComponent uiComponent, final Connector connector) {
        return connector.getName();
    }
}
