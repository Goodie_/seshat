package org.goodiemania.seshat.frontend.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.models.Attribute;

@Named
@ConversationScoped
public class EditItemBean implements Serializable {
    private String id;
    private Work work;
    private List<Attribute> metadata = new ArrayList<>();
    private List<Attribute> attributes = new ArrayList<>();
    private WorkType workType;

    /**
     * Load the specified Item into the bean.
     *
     * @param item Item object
     */
    public void load(final Item item) {
        this.id = item.getId();
        this.work = item.getWork();
        this.workType = item.getWork().getWorkType();

        this.metadata = new ArrayList<>();
        this.attributes = new ArrayList<>();

        workType.getMetadata().forEach(attribute -> {
            Object value = item.getMetaData().getOrDefault(attribute.getName(), attribute.getType().getDefaultSupplier().get());
            metadata.add(new Attribute(attribute, value));
        });

        workType.getAttributes().forEach(attribute -> {
            Object value = item.getWork().getAttributes().getOrDefault(attribute.getName(), attribute.getType().getDefaultSupplier().get());
            attributes.add(new Attribute(attribute, value));
        });
    }

    public String getId() {
        return id;
    }

    public List<Attribute> getMetadata() {
        return metadata;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public Work getWork() {
        return work;
    }

    /**
     * Converts Bean fields to Item.
     *
     * @return Converted Item
     */
    public Item toItem() {
        Map<String, Object> metadataMap = metadata.stream()
                .collect(Collectors.toMap(attribute -> attribute.getAttribute().getName(), Attribute::getValue));

        return new Item(id, work, metadataMap);
    }

    public String getName() {
        return work.getAttributes().get(workType.getAttributeName()).toString();
    }

    public String getDesc() {
        return work.getAttributes().get(workType.getAttributeDesc()).toString();
    }
}
