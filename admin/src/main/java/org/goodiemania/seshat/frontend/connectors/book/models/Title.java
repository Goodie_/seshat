package org.goodiemania.seshat.frontend.connectors.book.models;

public class Title {
    private String seriesTitle;
    private String title;
    private String subTitle;

    public String getSeriesTitle() {
        return seriesTitle;
    }

    public void setSeriesTitle(final String seriesTitle) {
        this.seriesTitle = seriesTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }
}
