package org.goodiemania.seshat.data;

import org.goodiemania.odin.external.EntityManager;
import org.goodiemania.seshat.data.dto.ImageDto;
import org.goodiemania.seshat.data.dto.ItemDto;
import org.goodiemania.seshat.data.dto.WorkDto;
import org.goodiemania.seshat.data.dto.WorkTypeDto;
import org.goodiemania.seshat.data.models.Image;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;

public class DtoConverter {
    private EntityManager<WorkTypeDto> workTypeEm;
    private EntityManager<WorkDto> workEm;

    public DtoConverter(final EntityManager<WorkTypeDto> workTypeEm, final EntityManager<WorkDto> workEm) {
        this.workTypeEm = workTypeEm;
        this.workEm = workEm;
    }

    /**
     * Converts from a WorkType to a WorkTypeDto for saving in the database.
     *
     * @param workType WorkType the front end uses
     * @return WorkTypeDto the database can use
     */
    public WorkTypeDto from(final WorkType workType) {
        return new WorkTypeDto(
                workType.getId(),
                workType.getName(),
                workType.getDescription(),
                workType.getAttributes(),
                workType.getMetadata(),
                workType.getAttributeName(),
                workType.getAttributeDesc(),
                workType.getConnectorInfo().orElse(null));
    }

    /**
     * Converts from a WorkTypeDto that the database uses to a WorkType for the front end.
     *
     * @param workTypeDto workTypeDto the database can use
     * @return WorkType the front end can use
     */
    public WorkType from(final WorkTypeDto workTypeDto) {
        return new WorkType(
                workTypeDto.getId(),
                workTypeDto.getName(),
                workTypeDto.getDescription(),
                workTypeDto.getAttributes(),
                workTypeDto.getMetadata(),
                workTypeDto.getAttributeName(),
                workTypeDto.getAttributeDesc(),
                workTypeDto.getConnectorInfo().orElse(null));
    }

    /**
     * Converts from a WorkDto that the database can use to a Work that the front end can use.
     *
     * <p>In the process it populates the ItemType field</p>
     *
     * @param workDto workDto object from the database
     * @return Work object for the front end
     */
    public Work from(final WorkDto workDto) {
        WorkTypeDto workTypeDto = workTypeEm.getById(workDto.getItemTypeId()).orElseThrow();

        Work work = new Work(
                workDto.getId(),
                from(workTypeDto),
                workDto.getAttributes(),
                workDto.getImageIds());

        workTypeDto.getAttributes().forEach(typeField ->
                work.getAttributes().putIfAbsent(
                        typeField.getName(),
                        typeField.getType().getDefaultSupplier().get()
                ));

        return work;
    }

    /**
     * Converts from a Work object for the front end to a WorkDto for the database.
     *
     * @param work Work object from the front end
     * @return WorkDto object for the database
     */
    public WorkDto from(final Work work) {
        return new WorkDto(
                work.getId(),
                work.getWorkType().getId(),
                work.getAttributes(),
                work.getImageIds());
    }

    /**
     * Converts from a Item object for the front end to a ItemDto for the database.
     *
     * @param item Item object from the front end
     * @return ItemDto object for the database
     */
    public ItemDto from(final Item item) {
        return new ItemDto(
                item.getId(),
                item.getWork().getId(),
                item.getMetaData());
    }

    /**
     * Converts from a ItemDto that the database can use to a Item that the front end can use.
     *
     * @param itemDto ItemDto object from the database
     * @return Item object for the front end
     */
    public Item from(final ItemDto itemDto) {
        WorkDto workDto = workEm.getById(itemDto.getWorkId()).orElseThrow();
        WorkTypeDto workTypeDto = workTypeEm.getById(workDto.getItemTypeId()).orElseThrow();

        Item item = new Item(
                itemDto.getId(),
                from(workDto),
                itemDto.getMetaData());

        workTypeDto.getMetadata().forEach(typeField ->
                item.getMetaData().putIfAbsent(
                        typeField.getName(),
                        typeField.getType().getDefaultSupplier().get()
                ));

        return item;
    }

    /**
     * Converts from a Image that the front end can use to a ImageDto the back end can use.
     *
     * @param image front end image object
     * @return Back end imageDto object
     */
    public ImageDto from(final Image image) {
        return new ImageDto(
                image.getId(),
                image.getContentTypeHeaders(),
                image.getBody());
    }

    /**
     * Converts from a ImageDto that the back end can use to a Image that the front end can use.
     *
     * @param image Back end imageDto object
     * @return front end image object
     */
    public Image from(final ImageDto image) {
        return new Image(
                image.getId(),
                image.getContentTypeHeaders(),
                image.getBody());
    }
}
