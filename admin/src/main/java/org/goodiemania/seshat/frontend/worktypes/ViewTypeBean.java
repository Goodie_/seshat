package org.goodiemania.seshat.frontend.worktypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.connectors.Connector;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;
import org.goodiemania.seshat.frontend.worktypes.model.ChooseConnectorSearchFields;
import org.goodiemania.seshat.frontend.worktypes.model.ConnectorMapping;

@Named
@ConversationScoped
public class ViewTypeBean implements Serializable {
    @Inject
    private ConnectorsController connectorsController;

    private String id;
    private String name;
    private String description;
    private List<WorkTypeField> attributes;
    private List<WorkTypeField> metadata;
    private String newAttrbName;
    private WorkTypeField.Type newAttrbType;
    private String newMetaName;
    private WorkTypeField.Type newMetaType;
    private String attributeName;
    private String attributeDesc;
    private Connector connector;
    private List<ConnectorMapping> connectorMappings;
    private String newConnectConnectName;
    private String newConnectWorkName;
    private List<ChooseConnectorSearchFields> searchFields;
    private String newSearchId;
    private List<String> newSearchFields;

    /**
     * Load the specified work type into the bean.
     *
     * @param type WorkType object
     */
    public void load(final WorkType type) {
        this.id = type.getId();
        this.name = type.getName();
        this.description = type.getDescription();
        this.attributes = type.getAttributes();
        this.metadata = type.getMetadata();
        this.attributeName = type.getAttributeName();
        this.attributeDesc = type.getAttributeDesc();

        type.getConnectorInfo().ifPresent(connectorInfo ->
                connectorsController.getByName(connectorInfo.getName()).ifPresent(foundConnector -> {
                    this.connector = foundConnector;
                    this.connectorMappings = connectorInfo.getConnectorMapping().stream()
                            .map(connectorMapping -> ConnectorMapping.of(
                                    connectorMapping.getConnectorName(),
                                    connectorMapping.getWorkName()))
                            .collect(Collectors.toList());
                    this.searchFields = connectorInfo.getSearchMapping()
                            .entrySet()
                            .stream()
                            .map((entry) -> ChooseConnectorSearchFields.of(entry.getKey(), entry.getValue()))
                            .collect(Collectors.toList());
                }));

        if (connectorMappings == null) {
            this.connectorMappings = new ArrayList<>();
        }

        resetNewAttribute();
        resetNewMetadata();
        resetNewConnect();
        resetNewConnectSearch();
    }

    /**
     * Reset the new attribute values back to default values.
     */
    public void resetNewAttribute() {
        newAttrbName = "";
        newAttrbType = WorkTypeField.Type.STRING;
    }

    /**
     * Reset the new metadata values back to default values.
     */
    public void resetNewMetadata() {
        newMetaName = "";
        newMetaType = WorkTypeField.Type.STRING;
    }

    /**
     * Reset the new metadata values back to default values.
     */
    public void resetNewConnect() {
        newConnectConnectName = "";
        newConnectWorkName = "";
    }


    /**
     * Reset the connector search fields.
     */
    public void resetNewConnectSearch() {
        newSearchId = "";
        newSearchFields = new ArrayList<>();
    }

    public WorkTypeField.Type[] getAttributeTypes() {
        return WorkTypeField.Type.values();
    }

    /**
     * Returns a list of the selected connector fields.
     *
     * @return Returns a list of connector fields
     */
    public List<String> getConnectorFields() {
        return Optional.ofNullable(connector)
                .map(Connector::getFields)
                .map(Map::keySet)
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of the selected mapping fields.
     *
     * @return Returns a list of mapping fields
     */
    public List<String> getConnectorMappingFields() {
        WorkTypeField.Type type = Optional.ofNullable(connector)
                .map(Connector::getFields)
                .map(stringTypeMap -> stringTypeMap.get(newConnectConnectName))
                .orElse(null);
        return attributes.stream()
                .filter(workTypeField -> workTypeField.getType().equals(type))
                .map(WorkTypeField::getName)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of the selected mapping fields.
     *
     * @return Returns a list of mapping fields
     */
    public List<String> getConnectorSearchFields() {
        Function<String, Boolean> hasSearchBeenUsed = s -> !searchFields.stream()
                .map(ChooseConnectorSearchFields::getSearchId)
                .collect(Collectors.toSet())
                .contains(s);

        return Optional.ofNullable(connector)
                .map(Connector::getSearchTypes)
                .stream()
                .flatMap(Collection::stream)
                .filter(hasSearchBeenUsed::apply)
                .collect(Collectors.toList());
    }

    /**
     * Returns a list of the selected mapping fields.
     *
     * @return Returns a list of mapping fields
     */
    public List<String> getConnectorSearchMappingFields() {
        return attributes.stream()
                .map(WorkTypeField::getName)
                .collect(Collectors.toList());
    }

    public List<Connector> getAllConnectors() {
        return connectorsController.getConnectors();
    }

    /**
     * Creates a ItemType object given this beans fields.
     *
     * @return The new ItemType
     */
    public WorkType toWorkType() {
        ConnectorInfo connectorInfo = null;

        if (connector != null) {
            HashMap<String, List<String>> searchFieldMap = new HashMap<>();
            searchFields.forEach(fields -> searchFieldMap.put(fields.getSearchId(), fields.getSearchFields()));

            connectorInfo = new ConnectorInfo(
                    connector.getName(),
                    this.connectorMappings.stream()
                            .map(connectorMapping -> new org.goodiemania.seshat.data.misc.ConnectorMapping(
                                    connectorMapping.getConnectorName(),
                                    connectorMapping.getWorkName()))
                            .collect(Collectors.toList()),
                    searchFieldMap);
        }

        return new WorkType(
                id,
                name,
                description,
                attributes,
                metadata,
                attributeName,
                attributeDesc,
                connectorInfo);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<WorkTypeField> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<WorkTypeField> attributes) {
        this.attributes = attributes;
    }

    public String getNewAttrbName() {
        return newAttrbName;
    }

    public void setNewAttrbName(final String newAttrbName) {
        this.newAttrbName = newAttrbName;
    }

    public WorkTypeField.Type getNewAttrbType() {
        return newAttrbType;
    }

    public void setNewAttrbType(final WorkTypeField.Type newAttrbType) {
        this.newAttrbType = newAttrbType;
    }

    public String getNewMetaName() {
        return newMetaName;
    }

    public void setNewMetaName(final String newMetaName) {
        this.newMetaName = newMetaName;
    }

    public WorkTypeField.Type getNewMetaType() {
        return newMetaType;
    }

    public void setNewMetaType(final WorkTypeField.Type newMetaType) {
        this.newMetaType = newMetaType;
    }

    public List<WorkTypeField> getMetadata() {
        return metadata;
    }

    public void setMetadata(final List<WorkTypeField> metadata) {
        this.metadata = metadata;
    }


    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(final String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeDesc() {
        return attributeDesc;
    }

    public void setAttributeDesc(final String attributeDesc) {
        this.attributeDesc = attributeDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Connector getConnector() {
        return connector;
    }

    public void setConnector(final Connector connector) {
        this.connector = connector;
    }

    public List<ConnectorMapping> getConnectorMappings() {
        return connectorMappings;
    }

    public void setConnectorMappings(List<ConnectorMapping> connectorMappings) {
        this.connectorMappings = connectorMappings;
    }

    public String getNewConnectConnectName() {
        return newConnectConnectName;
    }

    public void setNewConnectConnectName(final String newConnectConnectName) {
        this.newConnectConnectName = newConnectConnectName;
    }

    public String getNewConnectWorkName() {
        return newConnectWorkName;
    }

    public void setNewConnectWorkName(final String newConnectWorkName) {
        this.newConnectWorkName = newConnectWorkName;
    }

    public String getNewSearchId() {
        return newSearchId;
    }

    public void setNewSearchId(final String newSearchId) {
        this.newSearchId = newSearchId;
    }

    public List<String> getNewSearchFields() {
        return newSearchFields;
    }

    public void setNewSearchFields(final List<String> newSearchFields) {
        this.newSearchFields = newSearchFields;
    }

    public List<ChooseConnectorSearchFields> getSearchFields() {
        return searchFields;
    }

    public void setSearchFields(final List<ChooseConnectorSearchFields> searchFields) {
        this.searchFields = searchFields;
    }
}
