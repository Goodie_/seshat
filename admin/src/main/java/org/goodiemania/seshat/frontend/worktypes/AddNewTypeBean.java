package org.goodiemania.seshat.frontend.worktypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.goodiemania.seshat.data.models.WorkType;

@Named
@SessionScoped
public class AddNewTypeBean implements Serializable {
    private String name;
    private String description;
    private List<WorkTypeField> attributes;
    private List<WorkTypeField> metadata;
    private String newAttrbName;
    private WorkTypeField.Type newAttrbType;
    private String newMetaName;
    private WorkTypeField.Type newMetaType;
    private String attributeName;
    private String attributeDesc;

    @PostConstruct
    public void init() {
        reset();
    }

    public WorkTypeField.Type[] getAttributeTypes() {
        return WorkTypeField.Type.values();
    }

    /**
     * Creates a ItemType object given this beans fields.
     *
     * @return The new ItemType
     */
    public WorkType toWorkType() {
        return new WorkType(
                UUID.randomUUID().toString(),
                name,
                description,
                attributes,
                metadata,
                attributeName,
                attributeDesc,
                null);
    }

    /**
     * Reset the bean back to default values.
     */
    public void reset() {
        name = "";
        description = "";
        attributes = new ArrayList<>();
        metadata = new ArrayList<>();

        attributeDesc = "";
        attributeName = "";

        resetNewAttribute();
        resetNewMetadata();
    }

    /**
     * Reset the new attribute values back to default values.
     */
    public void resetNewAttribute() {
        newAttrbName = "";
        newAttrbType = WorkTypeField.Type.STRING;
    }

    /**
     * Reset the new metadata values back to default values.
     */
    public void resetNewMetadata() {
        newMetaName = "";
        newMetaType = WorkTypeField.Type.STRING;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<WorkTypeField> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<WorkTypeField> attributes) {
        this.attributes = attributes;
    }

    public String getNewAttrbName() {
        return newAttrbName;
    }

    public void setNewAttrbName(final String newAttrbName) {
        this.newAttrbName = newAttrbName;
    }

    public WorkTypeField.Type getNewAttrbType() {
        return newAttrbType;
    }

    public void setNewAttrbType(final WorkTypeField.Type newAttrbType) {
        this.newAttrbType = newAttrbType;
    }

    public String getNewMetaName() {
        return newMetaName;
    }

    public void setNewMetaName(final String newMetaName) {
        this.newMetaName = newMetaName;
    }

    public WorkTypeField.Type getNewMetaType() {
        return newMetaType;
    }

    public void setNewMetaType(final WorkTypeField.Type newMetaType) {
        this.newMetaType = newMetaType;
    }

    public List<WorkTypeField> getMetadata() {
        return metadata;
    }

    public void setMetadata(final List<WorkTypeField> metadata) {
        this.metadata = metadata;
    }


    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(final String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeDesc() {
        return attributeDesc;
    }

    public void setAttributeDesc(final String attributeDesc) {
        this.attributeDesc = attributeDesc;
    }
}
