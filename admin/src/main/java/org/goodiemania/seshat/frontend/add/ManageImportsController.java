package org.goodiemania.seshat.frontend.add;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.odin.external.model.SearchTerm;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.models.Image;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.NavigationController;
import org.goodiemania.seshat.frontend.connectors.Connector;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkFieldData;
import org.goodiemania.seshat.frontend.models.addwork.AddWorkFieldPossibleData;
import org.goodiemania.seshat.misc.Properties;

@Named
@ConversationScoped
public class ManageImportsController implements Serializable {
    private static final HttpClient httpClient = HttpClient.newBuilder().build();

    @Inject
    private WorkDao workDao;

    @Inject
    private ConnectorsController connectorsController;

    @Inject
    private SelectImportOptionsBean selectImportOptionsBean;

    @Inject
    private SelectDataSourceBean selectDataSourceBean;

    @Inject
    private NavigationController navigationController;

    private WorkType workType;
    private String searchType;
    private List<String> searchTerms;
    private ConnectorInfo connectorInfo;
    private Connector connector;
    private List<String> workSearchTerms;
    private String currentSearchTerm;


    /**
     * Starts the import process, for both single and bulk imports.
     *
     * @param workType    Type of work
     * @param searchType  Search type.
     * @param searchTerms Search terms.
     * @return Redirecion string to get started
     */
    public String start(
            final WorkType workType,
            final String searchType,
            final List<String> searchTerms) {
        if (searchTerms.isEmpty()) {
            throw new IllegalArgumentException("Must pass at least one search term");
        }

        this.workType = workType;
        this.searchType = searchType;
        this.searchTerms = new ArrayList<>();
        this.searchTerms.addAll(searchTerms);
        this.connectorInfo = workType.getConnectorInfo().orElse(null);
        this.connector = workType.getConnectorInfo()
                .map(ConnectorInfo::getName)
                .flatMap(connectorName -> connectorsController.getByName(connectorName))
                .orElse(null);

        if (connector == null || connectorInfo == null) {
            return navigationController.addItem();
        }

        List<String> searchTermsBasic = connectorInfo.getSearchMapping().get(searchType);
        workSearchTerms = Collections.emptyList();
        if (searchTermsBasic != null) {
            workSearchTerms = searchTermsBasic
                    .stream()
                    .map(searchField -> "WorkDto_attributes_" + searchField)
                    .collect(Collectors.toList());
        }

        return nextStep();
    }

    /**
     * Sets up and redirects to the next step in the import process.
     *
     * @return redirect string
     */
    public String nextStep() {
        if (searchTerms.isEmpty()) {
            return navigationController.addItem();
        } else {
            this.currentSearchTerm = this.searchTerms.get(0);
            this.searchTerms.remove(0);

            List<Work> searchResults = workDao.searchWorks(
                    workSearchTerms.stream()
                            .map(searchField -> SearchTerm.of(searchField, currentSearchTerm))
                            .collect(Collectors.toList()));

            if (searchResults.isEmpty()) {
                //TODO handle option where connector doesn't find the work
                return selectFromConnector();
            } else {
                selectImportOptionsBean.setPossibleWorks(searchResults);
                return "selectFromWorks?faces-redirect=true";
            }
        }
    }

    public String selectFromConnector() {
        selectDataSourceBean.load(workType, searchType, currentSearchTerm);
        return "selectImportOptions?faces-redirect=true";
    }

    public String getSearchType() {
        return searchType;
    }

    public String getCurrentSearchTerm() {
        return currentSearchTerm;
    }

    /**
     * Takes the selected work from the front and creates a new item for it.
     *
     * @param work selected work
     * @return Redirection string for the next step in the import process
     */
    public String addItemToWork(final Work work) {
        Item item = new Item(UUID.randomUUID().toString(),
                work,
                new HashMap<>()
        );
        workDao.save(item);

        return nextStep();
    }

    /**
     * Adds a new work, and item, redirects to the next step in import.
     *
     * @return redirection string
     */
    public String addNewWorkItem() {
        Work work = createAndSaveWork();
        workDao.save(new Item(UUID.randomUUID().toString(), work, Collections.emptyMap()));

        return nextStep();
    }

    private Work createAndSaveWork() {
        //TODO we need to audit what came from where, I've gone to enough lengths to source here so...
        HashMap<String, Object> fieldsMap = new HashMap<>();
        selectDataSourceBean.getFields().forEach(field -> {
            if (field.getValue() != null) {
                fieldsMap.put(field.getFieldName(), field.getValue().getValue());
            }
        });
        selectDataSourceBean.getListFields().forEach(field -> {
            if (field.getValue() != null) {
                List<String> collectedValues = field.getValue().stream()
                        .map(AddWorkFieldData::getValue)
                        .collect(Collectors.toList());
                fieldsMap.put(field.getFieldName(), collectedValues);
            }
        });
        List<String> imageIds = selectDataSourceBean.getImageFields().stream()
                .filter(AddWorkFieldPossibleData::isSelected)
                .map(AddWorkFieldPossibleData::getValue)
                .map(this::parseImage)
                .collect(Collectors.toList());


        Work work = new Work(
                UUID.randomUUID().toString(),
                selectDataSourceBean.getWorkType(),
                fieldsMap,
                imageIds);

        workDao.save(work);

        return work;
    }

    private String parseImage(final String url) {
        URI uri = URI.create(url);
        HttpRequest request = HttpRequest.newBuilder().GET()
                .uri(uri)
                .setHeader("authorization", Properties.BOOK_INFORMATION_API_KEY.get().orElseThrow())
                .build();

        HttpResponse<byte[]> send = null;
        try {
            send = httpClient.send(request, HttpResponse.BodyHandlers.ofByteArray());
        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException(e);
        }

        List<String> contentTypeHeaders = send.headers().allValues("Content-Type");
        byte[] body = send.body();

        Image image = new Image(UUID.randomUUID().toString(), contentTypeHeaders, body);

        workDao.save(image);

        return image.getId();
    }
}
