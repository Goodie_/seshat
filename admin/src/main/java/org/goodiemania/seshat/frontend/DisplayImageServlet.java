package org.goodiemania.seshat.frontend;

import java.io.IOException;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.models.Image;


@WebServlet("/image/*")
public class DisplayImageServlet extends HttpServlet {
    @Inject
    private WorkDao workDao;

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        String requestedImage = req.getPathInfo().substring(1);

        if (StringUtils.isBlank(requestedImage)) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        Optional<Image> image = workDao.getImageById(requestedImage);
        if (image.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        image.get().getContentTypeHeaders().forEach(contentType -> resp.addHeader("Content-Type", contentType));

        ServletOutputStream out = resp.getOutputStream();
        out.write(image.get().getBody());
        out.close();
    }
}
