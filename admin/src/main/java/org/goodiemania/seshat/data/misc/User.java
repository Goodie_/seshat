package org.goodiemania.seshat.data.misc;

public class User {
    private final String id;
    private final String username;
    private final boolean canEditItems;
    private final boolean canEditWorks;
    private final boolean canEditWorkTypes;

    /**
     * Creates a instance of User object.
     *
     * @param id               Id of the user
     * @param username         users Username
     * @param canEditItems     If they can edit items
     * @param canEditWorks     if they can edit works
     * @param canEditWorkTypes iif they can edit work types
     */
    public User(final String id, final String username, final boolean canEditItems, final boolean canEditWorks, final boolean canEditWorkTypes) {
        this.id = id;
        this.username = username;
        this.canEditItems = canEditItems;
        this.canEditWorks = canEditWorks;
        this.canEditWorkTypes = canEditWorkTypes;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public boolean isCanEditItems() {
        return canEditItems;
    }

    public boolean isCanEditWorks() {
        return canEditWorks;
    }

    public boolean isCanEditWorkTypes() {
        return canEditWorkTypes;
    }
}
