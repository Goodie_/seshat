package org.goodiemania.seshat.frontend.worktypes;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.misc.WorkTypeField;
import org.omnifaces.util.Faces;

@Named
@RequestScoped
public class AddNewTypeController {
    @Inject
    private AddNewTypeBean addNewTypeBean;

    @Inject
    private WorkDao workDao;


    public void removeAttribute(final WorkTypeField attribute) {
        addNewTypeBean.getAttributes().remove(attribute);
    }


    public void removeMetadata(final WorkTypeField metadata) {
        addNewTypeBean.getMetadata().remove(metadata);
    }

    /**
     * Adds the currently entered attribute type to the bean.
     */
    public void addAttribute() {
        if (StringUtils.isNotBlank(addNewTypeBean.getNewAttrbName())) {
            WorkTypeField newAttribute = new WorkTypeField(
                    addNewTypeBean.getNewAttrbName(),
                    addNewTypeBean.getNewAttrbType());
            addNewTypeBean.getAttributes().add(newAttribute);
            addNewTypeBean.resetNewAttribute();
        }
    }

    /**
     * Adds the currently entered metadata type to the bean.
     */
    public void addMetadata() {
        if (StringUtils.isNotBlank(addNewTypeBean.getNewMetaName())) {
            WorkTypeField newAttribute = new WorkTypeField(
                    addNewTypeBean.getNewMetaName(),
                    addNewTypeBean.getNewMetaType());
            addNewTypeBean.getMetadata().add(newAttribute);
            addNewTypeBean.resetNewMetadata();
        }
    }

    /**
     * Saves Whatever is currently in the add new type bean.
     *
     * @return String to redirect the browser with
     */
    public String save() {
        workDao.save(addNewTypeBean.toWorkType());
        addNewTypeBean.reset();

        Faces.getContext().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "New Work type added", "")
        );
        Faces.getExternalContext().getFlash().setKeepMessages(true);

        return "browse?faces-redirect=true";
    }
}
