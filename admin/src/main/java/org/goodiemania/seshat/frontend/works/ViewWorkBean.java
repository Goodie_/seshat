package org.goodiemania.seshat.frontend.works;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.models.Attribute;
import org.goodiemania.seshat.frontend.models.KeyValueMap;

@Named
@ConversationScoped
public class ViewWorkBean implements Serializable {
    private String id;
    private Work work;
    private WorkType workType;
    private List<Attribute> attributes = new ArrayList<>();

    private List<Item> items = new ArrayList<>();

    /**
     * Load the specified Item into the bean.
     *
     * @param work  Work object
     * @param items Items associated with said work
     */
    public void load(final Work work, final List<Item> items) {
        this.id = work.getId();
        this.work = work;
        this.workType = work.getWorkType();
        this.items = items;

        this.attributes = new ArrayList<>();

        work.getWorkType().getAttributes().forEach(attribute -> {
            Object value = work.getAttributes().getOrDefault(attribute.getName(), attribute.getType().getDefaultSupplier().get());
            attributes.add(new Attribute(attribute, value));
        });
    }

    public String getId() {
        return id;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public String getName() {
        return work.getAttributes().get(workType.getAttributeName()).toString();
    }

    public String getDesc() {
        return work.getAttributes().get(workType.getAttributeDesc()).toString();
    }

    public List<Item> getItems() {
        return items;
    }

    /**
     * Creates a list of KeyValueMaps from the given items metadata.
     *
     * @param item The given item
     * @return a list of key value pairs
     */
    public String metadata(final Item item) {
        if (item == null) {
            return "";
        }

        return item.getMetaData()
                .entrySet()
                .stream()
                .map(entry -> KeyValueMap.of(entry.getKey(), entry.getValue().toString()))
                .findFirst()
                .map(KeyValueMap::getKey)
                .orElse("");
    }
}
