package org.goodiemania.seshat.frontend.connectors.book;

import java.util.List;

final class KeyListOfStringHolder {
    final String key;
    final List<String> value;

    KeyListOfStringHolder(String k, List<String> v) {
        this.key = k;
        this.value = v;
    }

    public String getKey() {
        return this.key;
    }

    public List<String> getValue() {
        return this.value;
    }
}