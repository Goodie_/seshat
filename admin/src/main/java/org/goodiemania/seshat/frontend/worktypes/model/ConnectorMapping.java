package org.goodiemania.seshat.frontend.worktypes.model;

public class ConnectorMapping {
    private String connectorName;
    private String workName;

    /**
     * Creates a new instance of a GuiConnectorMapping.
     *
     * @param connectorName Name of variable provided by the connector.
     * @param workName      Name of the variable belonging to the Work
     * @return A mapping containing both.
     */
    public static ConnectorMapping of(final String connectorName, final String workName) {
        ConnectorMapping connectorMapping = new ConnectorMapping();
        connectorMapping.setConnectorName(connectorName);
        connectorMapping.setWorkName(workName);

        return connectorMapping;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }
}
