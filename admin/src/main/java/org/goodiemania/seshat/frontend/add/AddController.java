package org.goodiemania.seshat.frontend.add;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.Conversation;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.frontend.NavigationController;

@Named
public class AddController {
    @Inject
    private WorkDao workDao;

    @Inject
    private NavigationController navigationController;

    @Inject
    private Conversation conversation;

    @Inject
    private InitAddBean initAddBean;

    @Inject
    private SelectDataSourceBean selectDataSourceBean;

    @Inject
    private ManageImportsController importsController;

    /**
     * Begin add process via controller.
     *
     * @return Redirection string
     */
    public String addViaController() {
        if (conversation.isTransient()) {
            conversation.begin();
        }

        String searchType = initAddBean.getConnectorSearchType();
        List<String> searchTerms = Arrays.stream(initAddBean.getConnectorSearchTerm().split(","))
                .flatMap(s -> Arrays.stream(s.split("\\r?\\n")))
                .collect(Collectors.toList());

        return importsController.start(initAddBean.getWorkType(), searchType, searchTerms);
    }
}
