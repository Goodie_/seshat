package org.goodiemania.seshat.frontend.add;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.models.WorkType;
import org.goodiemania.seshat.frontend.connectors.Connector;
import org.goodiemania.seshat.frontend.connectors.ConnectorsController;

@Named
@ViewScoped
public class InitAddBean implements Serializable {
    @Inject
    private WorkDao workDao;

    @Inject
    private ConnectorsController connectorsController;

    private List<WorkType> availableWorkTypes;
    private WorkType workType;
    private String connectorSearchType;
    private String connectorSearchTerm;

    @PostConstruct
    public void init() {
        availableWorkTypes = workDao.getAllWorkTypes();
    }

    public List<WorkType> getAvailableWorkTypes() {
        return availableWorkTypes;
    }

    public void setAvailableWorkTypes(final List<WorkType> availableWorkTypes) {
        this.availableWorkTypes = availableWorkTypes;
    }

    public WorkType getWorkType() {
        return workType;
    }

    public void setWorkType(final WorkType workType) {
        this.workType = workType;
    }

    public ConnectorInfo getConnectorInfo() {
        return workType.getConnectorInfo().orElse(null);
    }

    public Connector getConnector() {
        return connectorsController.getByName(getConnectorInfo().getName()).orElse(null);
    }

    public String getConnectorSearchType() {
        return connectorSearchType;
    }

    public void setConnectorSearchType(final String connectorSearchType) {
        this.connectorSearchType = connectorSearchType;
    }

    public String getConnectorSearchTerm() {
        return connectorSearchTerm;
    }

    public void setConnectorSearchTerm(final String connectorSearchTerm) {
        this.connectorSearchTerm = connectorSearchTerm;
    }
}
