package org.goodiemania.seshat.data.misc;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Supplier;

public class WorkTypeField {
    private final String name;
    private final Type type;

    /**
     * Creates a new instance of a Attribute for a work type.
     *
     * @param name The identifying name of the attribute
     * @param type What type of attribute is this? A string? A list? (Might add more later)
     */
    @JsonCreator
    public WorkTypeField(
            @JsonProperty("name") final String name,
            @JsonProperty("type") final Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WorkTypeField that = (WorkTypeField) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public enum Type {
        STRING("Value", () -> ""),
        LIST("List of values", ArrayList::new);

        private String description;
        private Supplier<Object> defaultSupplier;

        Type(final String description, final Supplier<Object> defaultSupplier) {
            this.description = description;
            this.defaultSupplier = defaultSupplier;
        }

        public String getDescription() {
            return description;
        }

        public Supplier<Object> getDefaultSupplier() {
            return defaultSupplier;
        }
    }
}
