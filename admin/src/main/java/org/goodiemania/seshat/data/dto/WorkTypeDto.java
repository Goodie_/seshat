package org.goodiemania.seshat.data.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;
import org.goodiemania.odin.external.annotations.Entity;
import org.goodiemania.odin.external.annotations.Id;
import org.goodiemania.odin.external.annotations.Index;
import org.goodiemania.seshat.data.misc.ConnectorInfo;
import org.goodiemania.seshat.data.misc.WorkTypeField;

@Entity
@Index
public class WorkTypeDto {
    @Id
    private final String id;
    private final String name;
    private final String description;
    private final List<WorkTypeField> attributes;
    private final List<WorkTypeField> metadata;
    private final String attributeName;
    private final String attributeDesc;
    private final ConnectorInfo connectorInfo;


    /**
     * Constructs a new ItemTypeDto, given the field parameters, annotated for Jackson's use.
     *
     * @param id            Id of the object
     * @param name          Name of the ItemType
     * @param description   Description of the ItemType
     * @param attributes    Required attributes of the WorkType
     * @param metadata      Required metadata of the Item
     * @param attributeName Attribute that will be used as a works name
     * @param attributeDesc Attribute that will be used as a works description
     * @param connectorInfo Information about connecting to a connector
     */
    @JsonCreator
    public WorkTypeDto(
            @JsonProperty("id") final String id,
            @JsonProperty("name") final String name,
            @JsonProperty("description") final String description,
            @JsonProperty("attributes") final List<WorkTypeField> attributes,
            @JsonProperty("metadata") final List<WorkTypeField> metadata,
            @JsonProperty("attribute_name") final String attributeName,
            @JsonProperty("attribute_desc") final String attributeDesc,
            @JsonProperty("connectorInfo") final ConnectorInfo connectorInfo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.attributes = attributes;
        this.metadata = metadata;
        this.attributeName = attributeName;
        this.attributeDesc = attributeDesc;
        this.connectorInfo = connectorInfo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<WorkTypeField> getAttributes() {
        return attributes;
    }

    public String getDescription() {
        return description;
    }

    public List<WorkTypeField> getMetadata() {
        return metadata;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getAttributeDesc() {
        return attributeDesc;
    }

    public Optional<ConnectorInfo> getConnectorInfo() {
        return Optional.ofNullable(connectorInfo);
    }
}
