package org.goodiemania.seshat.frontend.search;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.goodiemania.seshat.data.WorkDao;
import org.goodiemania.seshat.data.models.Item;
import org.goodiemania.seshat.data.models.Work;

@Named
@RequestScoped
public class SearchController {
    private Random random = new Random();
    private BigDecimal divisor = BigDecimal.valueOf(1000);

    @Inject
    private SearchBean searchBean;

    @Inject
    private WorkDao workDao;

    /**
     * Redirect to the new search location.
     *
     * @return redirection string
     */
    public String newSearch() {
        if (StringUtils.isBlank(searchBean.getSearchString())) {
            return "";
        }

        return String.format("/home?q=%s&t=%s&faces-redirect=true",
                URLEncoder.encode(searchBean.getSearchString(), StandardCharsets.UTF_8),
                searchBean.getSearchType());
    }

    /**
     * Performs a search if we have a search string set.
     */
    public void search() {
        if (StringUtils.isBlank(searchBean.getSearchString())) {
            return;
        }

        BigDecimal timeTaken = timeEvent(this::internalSearch);
        searchBean.setSearchTime(timeTaken);
    }

    private void internalSearch() {
        List<SearchResult> results = Collections.emptyList();

        if (searchBean.getSearchType() == SearchType.ITEMS) {
            results = workDao.searchItems(searchBean.getSearchString())
                    .stream()
                    .map(this::createSearchResult)
                    .collect(Collectors.toList());
        } else if (searchBean.getSearchType() == SearchType.WORKS) {
            results = workDao.searchWorks(searchBean.getSearchString())
                    .stream()
                    .map(this::createSearchResult)
                    .collect(Collectors.toList());
        }

        searchBean.setSearchResults(results);
    }

    /**
     * Redirect to the new search location, with a new search type.
     *
     * @return redirection string
     */
    public String searchItems() {
        return String.format("/home?q=%s&t=%s&faces-redirect=true",
                searchBean.getSearchString(),
                SearchType.ITEMS);
    }

    /**
     * Redirect to the new search location, with a new search type.
     *
     * @return redirection string
     */
    public String searchWorks() {
        return String.format("/home?q=%s&t=%s&faces-redirect=true",
                searchBean.getSearchString(),
                SearchType.WORKS);
    }


    private BigDecimal timeEvent(final Runnable runnable) {
        long start = System.currentTimeMillis();
        runnable.run();
        BigDecimal time = BigDecimal.valueOf(System.currentTimeMillis() - start);
        return time.setScale(2, RoundingMode.HALF_EVEN).divide(divisor, RoundingMode.HALF_EVEN);
    }

    private SearchResult createSearchResult(final Item item) {
        String imageUrl = item.getWork().getImageIds()
                .stream()
                .map(id -> String.format("/image/%s", id))
                .findFirst()
                .orElseGet(this::createPlaceHolderImageUrl);

        return createSearchResult(
                imageUrl,
                item.getName(),
                String.format("Item (%s)", item.getWork().getWorkType().getName()),
                item.getId());
    }

    private SearchResult createSearchResult(final Work work) {
        String imageUrl = work.getImageIds()
                .stream()
                .map(id -> String.format("/image/%s", id))
                .findFirst()
                .orElseGet(this::createPlaceHolderImageUrl);

        return createSearchResult(
                imageUrl,
                work.getName(),
                work.getWorkType().getName(),
                work.getId());
    }

    private SearchResult createSearchResult(final String imageUrl, final String name, final String typeName, final String id) {
        if (StringUtils.isBlank(imageUrl)) {
            return new SearchResult(createPlaceHolderImageUrl(), name, typeName, id);
        } else {
            return new SearchResult(imageUrl, name, typeName, id);
        }
    }

    private String createPlaceHolderImageUrl() {
        return "https://placekitten.com/g/30" + random.nextInt(9) + "/40" + random.nextInt(9);
    }
}
